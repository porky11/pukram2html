use pukram2html::convert;

#[test]
fn simple() {
    let pukram_text = "# Welcome to Pukram\nThis is a *simple* and /intuitive/ markup language.";
    let mut html_output = Vec::new();
    let text_info = convert(pukram_text.lines(), &mut html_output);

    assert_eq!(
        std::str::from_utf8(&html_output).unwrap(),
        "<h1>Welcome to Pukram</h1><p>\nThis is a <b>simple</b> and <i>intuitive</i> markup language.</p>\n"
    );
    assert_eq!(text_info.lines, 1);
    assert_eq!(text_info.words, 7);
    assert_eq!(text_info.chars, 47);
}
