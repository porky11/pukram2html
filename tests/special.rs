use pukram2html::convert;

#[test]
fn special() {
    let pukram_text = r"
    - Link: Example-Link https://example.com
    - Image: example.jpg https://example.com/image.jpg
    - Audio: https://example.com/audio.mp3
    ";
    let mut html_output: Vec<u8> = Vec::new();
    let text_info = convert(pukram_text.lines(), &mut html_output);

    assert_eq!(
        std::str::from_utf8(&html_output).unwrap(),
        "<p>\n<a href=\"https://example.com\">Example-Link</a>\n<br>\n<img src=\"https://example.com/image.jpg\" alt=\"example.jpg\" style='max-height: 100%; max-width: 100%; object-fit: cover'>\n<br>\n<audio controls src=\"https://example.com/audio.mp3\"/>\n</p>\n"
    );
    assert_eq!(text_info.lines, 0);
    assert_eq!(text_info.words, 0);
    assert_eq!(text_info.chars, 0);
}
