use pukram2html::{Settings, convert_extended};

#[test]
fn extended() {
    let pukram_text = "# Welcome to Pukram\nThis is a *simple* and /intuitive/ markup language.";
    let mut html_output = Vec::new();
    let settings = Settings::default()
        .with_start_level(2)
        .with_use_textboxes(true);
    let text_info = convert_extended(pukram_text.lines(), &mut html_output, settings);

    assert_eq!(
        std::str::from_utf8(&html_output).unwrap(),
        "<h3>Welcome to Pukram</h3><p>\nThis is a <b>simple</b> and <i>intuitive</i> markup language.</p>\n"
    );
    assert_eq!(text_info.lines, 1);
    assert_eq!(text_info.words, 7);
    assert_eq!(text_info.chars, 47);
}
