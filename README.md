# pukram2html

## Introduction

The `pukram2html` library is a Rust library that converts text written in the Pukram markup language to HTML. Pukram is a simple and intuitive markup language inspired by Markdown, designed for easy formatting of text content. The library is primarily targeted at developers building web servers who need to convert Pukram-formatted text into HTML for displaying on websites. Additionally, it can be used to convert Pukram text files to HTML, which can then be further converted to PDF using other tools.

## Pukram Markup Language

Pukram is a lightweight markup language that supports basic formatting and structural elements. Here are the key features of Pukram:

### Headers

Headers in Pukram are created by starting a line with one or more `#` characters. The number of `#` determines the header level (e.g., `#` for `<h1>`, `##` for `<h2>`, etc.).

### Commands

Commands in Pukram are created by starting a line with `-` for default commands or `+` for user defined commands.
Commands can be nested by increasing the indentation level.
Fake commands start with `=`.

### Formatting Markups

Pukram supports the following formatting markups:

- **Normal**: Plain text without any formatting.
- **Bold**: Text surrounded by `*` characters (e.g., `*bold*`).
- **Italic**: Text surrounded by `/` characters (e.g., `/italic/`).
- **Monospace**: Text surrounded by `` ` `` characters (e.g., `` `mono` ``).
- **Superscript**: Text surrounded by `^` characters (e.g., `^superscript^`).
- **Subscript**: Text surrounded by `|` characters (e.g., `|subscript|`).
- **Small**: Text surrounded by `^|` and `|^` delimiters (e.g., `^|small|^`).
- **Underscore**: Text surrounded by `_` characters (e.g., `_underscore_`).
- **Strikethrough**: Text surrounded by `~` characters (e.g., `~strikethrough~`).

Note that formatting is reset at the end of each line, so you can apply formatting to an entire line without needing closing delimiters.

## Library Structure

The `pukram2html` library consists of several components that work together to convert Pukram text to HTML:

- `TextInfo` struct: Holds information about the converted text, such as the number of lines, words, and characters.
- `convert` function: The main entry point for converting Pukram text to HTML.
- `convert_subheader` function: Converts Pukram text to HTML with subheaders.
- `convert_extended` function: Converts Pukram text to HTML with extended settings.
- `Settings` struct: Holds configuration settings for the conversion process.
- `FormattingExtensions` trait: Provides methods for handling formatting markups.

## API Reference

The API reference documentation provides detailed information about each module, struct, trait, and function in the `pukram2html` library, including their parameters, return values, and behavior. Consult the API reference for a comprehensive understanding of the library's components and how to use them effectively.

## Troubleshooting

If you encounter any issues while using the `pukram2html` library, refer to the troubleshooting section for common problems and their solutions. The FAQs section addresses frequently asked questions and provides additional guidance on using the library.

If you still have questions or need further assistance, don't hesitate to reach out to the library's maintainers or the Rust community for support.
